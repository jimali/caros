#include <caros/pose_sensor_si_proxy.h>
#include <caros/button_sensor_si_proxy.h>
#include <caros/common.h>

#include <rw/math/Q.hpp>

#include <ros/ros.h>

#include <string>
#include <vector>
#include <cassert>

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "trakstar_simple_demo");

    caros::PoseSensorSIProxy psensor("trakstar");
    caros::ButtonSensorSIProxy bsensor("trakstar");

    // check if sensors are online
    // hmm, no way to do this. Perhaps with a caros node si proxy???

    // read sensors 100 times and exit
    for(int i=0;i<100;i++){
    	std::vector<caros::PoseSensorSIProxy::PoseData> poses = psensor.getPoses();

    	ROS_INFO_STREAM("Recieved: " << poses.size() << " poses!");
    	for(int j=0;j<poses.size();j++){
    		ROS_INFO_STREAM( "sensor id: " << poses[j].id );
    		ROS_INFO_STREAM( "pose     : " << poses[j].pose );
    		ROS_INFO_STREAM( "quality  : " << poses[j].quality );
    		ROS_INFO_STREAM( "frame    : " << poses[j].frame );
    	}

    	std::vector<caros::ButtonSensorSIProxy::ButtonData> buttons = bsensor.getButtons();
    	ROS_INFO_STREAM("Recieved: " << buttons.size() << " buttons!");
    	for(int j=0;j<buttons.size();j++){
    		ROS_INFO_STREAM( "sensor id: " << buttons[j].id );
    		ROS_INFO_STREAM( "float val: " << buttons[j].button);
    		ROS_INFO_STREAM( "isAnalog : " << buttons[j].isAnalog );
    		ROS_INFO_STREAM( "stamp    : " << buttons[j].stamp );
    	}
    }

    return 0;
}
